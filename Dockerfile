# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM ubuntu

RUN apt-get update && \
    apt-get install -y golang-go librdkafka-dev git
# Copy the local package files to the container's workspace.
ADD ./producer.go /go/src/github.com/hdf-kafka-app/
ADD ./telemetry.csv /go/src/github.com/hdf-kafka-app/
ADD ./telemetry.avro /go/src/github.com/hdf-kafka-app/

ENV brokers "0.0.0.0:6667"

ENV topic "telemetry"

# Install all dependecy here for now
#RUN go get github.com/Shopify/sarama
RUN go get -u github.com/confluentinc/confluent-kafka-go/kafka

# Build the outyet command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go install github.com/hdf-kafka-app

# Run the outyet command by default when the container starts.
ENTRYPOINT /go/bin/hdf-kafka-app

 
