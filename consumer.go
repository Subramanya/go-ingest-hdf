package main

import (
  //"fmt"
  "github.com/Shopify/sarama"
  "os"
  "os/signal"
  //"strings"
  "log"
)


func main(){
  consumer, err := sarama.NewConsumer([]string{"sandbox-hdf.hortonworks.com:6667"}, nil)
  if err != nil {
      panic(err)
  }
  
  defer func() {
      if err := consumer.Close(); err != nil {
          log.Fatalln(err)
      }
  }()
  
  partitionConsumer, err := consumer.ConsumePartition("telemetry2", 0, sarama.OffsetOldest)
  if err != nil {
      panic(err)
  }
  
  defer func() {
      if err := partitionConsumer.Close(); err != nil {
          log.Fatalln(err)
      }
  }()
  
  // Trap SIGINT to trigger a shutdown.
  signals := make(chan os.Signal, 1)
  signal.Notify(signals, os.Interrupt)
  
  consumed := 0
  ConsumerLoop:
  for {
      select {
      case msg := <-partitionConsumer.Messages():
          log.Printf("Consumed message %s offset %d\n", msg.Value, msg.Offset)
          consumed++
      case <-signals:
          break ConsumerLoop
      }
  }
  
  log.Printf("Consumed: %d\n", consumed)
}

