package main

import (
  "fmt"
  "strings"
)


var messages = make(chan string)


func send_msg(){

  messages <- "hello world" 

}


func main(){
  go send_msg()

  msg := <- messages
  fmt.Println(strings.ToUpper(msg))

}



