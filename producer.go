package main

import (
  "github.com/confluentinc/confluent-kafka-go/kafka"
  "log"
  "time"
  "io"
  "os"
  "bufio"
  "encoding/csv"
  "fmt"
  "strconv"
)

func check(e error){
  if e != nil{
    panic(e)
  }
}


func main(){
  
  // Get brokers from environment variable
  brokers := os.Getenv("brokers")
  
  // Get topic name from environment variable
  topic := os.Getenv("topic")
  
  // connection to brokers
  producer, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": brokers})

  // if there are any errors log them
  if err != nil {
      log.Fatalln(err)
  }


  // Close the connection to broker while exiting main method
  defer producer.Close()
       
  //Headers: 0"dates",1"machineID",2"volt",3"rotate",4"pressure",5"vibration",6"dates.errors",7"type1s",8"type2s",9"type3s",10"type4s",11"type5s",12"dates.failures",13"comp1s",14"comp2s",15"comp3s",16"comp4s"
  telemetry_dataset,_:= os.Open("newfulldata.csv")

  // Read csv file 
  // reader := bufio.NewReader(telemetry_dataset)
  reader := csv.NewReader(bufio.NewReader(telemetry_dataset))

  
  // Start reading the telemetry data line by line
  for {
    // line,err := reader.ReadString('\n')

    line,err := reader.Read()

    //datetime := strconv.FormatInt(time.Now().UnixNano()/1000000,10)
    datetime,_ := strconv.ParseInt(line[0],10,64)
    machineid,_ := strconv.ParseInt(line[1], 10, 32)
    volt,_ := strconv.ParseFloat(line[2],32)
    rotate,_ := strconv.ParseFloat(line[3],32)
    pressure,_ := strconv.ParseFloat(line[4],32)
    vibration,_ := strconv.ParseFloat(line[5],32)
    type1s,_ := strconv.ParseInt(line[7], 10, 32)
    type2s,_ := strconv.ParseInt(line[8], 10, 32)
    type3s,_ := strconv.ParseInt(line[9], 10, 32)
    type4s,_ := strconv.ParseInt(line[10], 10, 32)
    type5s,_ := strconv.ParseInt(line[11], 10, 32)
    comp1s,_ := strconv.ParseInt(line[13], 10, 32)
    comp2s,_ := strconv.ParseInt(line[14], 10, 32)
    comp3s,_ := strconv.ParseInt(line[15], 10, 32)
    comp4s,_ := strconv.ParseInt(line[16], 10, 32)


    if err == io.EOF{
      break
    }

   if err != nil{
      log.Fatalln(err)
    }
    
    formated_line := strconv.FormatInt(datetime * 1000000,10) + "," + strconv.FormatInt(machineid,10) + ","  + strconv.FormatFloat(volt,'f',-1,64) + "," + strconv.FormatFloat(rotate ,'f',-1,64)+ "," + strconv.FormatFloat(pressure ,'f',-1,64)+ "," + strconv.FormatFloat(vibration ,'f',-1,64)  + "," + strconv.FormatInt(type1s,10) + "," + strconv.FormatInt(type2s,10) + "," + strconv.FormatInt(type3s,10) + "," + strconv.FormatInt(type4s,10) + "," + strconv.FormatInt(type5s,10) + "," + strconv.FormatInt(comp1s,10) + "," + strconv.FormatInt(comp2s,10) + "," + strconv.FormatInt(comp3s,10) + "," + strconv.FormatInt(comp4s,10)
      
   
    // append the age of machine - 1
    formated_line += "," + strconv.Itoa(int(495))

    fmt.Println(formated_line)
    
    producer.Produce(&kafka.Message{
       		     TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		     Value:          []byte(formated_line),
		}, nil)


     if err != nil {
         log.Printf("FAILED to send message: %s\n", err)
     } else {
         //log.Printf("> message sent to partition %d at offset %d\n", partition, offset)
     }
     // sleep for 1second before sending another message
     time.Sleep(1000 * time.Millisecond)
  }
}
